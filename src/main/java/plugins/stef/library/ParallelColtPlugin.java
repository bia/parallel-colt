package plugins.stef.library;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Full Parallel Colt library for Icy.<br>
 * It also embed JTransforms, CSparseJ, Optimization, JPlasma, Netlib-java, Arpack-combo and JUnit libraries.
 * 
 * @author Stephane Dallongeville
 */
public class ParallelColtPlugin extends Plugin implements PluginLibrary
{
    //
}
